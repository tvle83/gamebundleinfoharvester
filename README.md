# IndieGala Key Getter


Since I found out about IndieGala I have loved the idea and have put in quite a bit of money towards the charities and publishers of these great games they offer.

I have multiple copies of each bundle and found myself wanting to easily retrieve all the keys and download links of the games and music that are available from some of the bundles.

I wrote this bookmarklet to add a text box and a tab delimited list of all the games, music, drm free games, and android games of the bundle on the current page.

I am releasing this to the public in the hopes that it will be useful to others. Also, if IndieGala changes up their code I hope to have some help updating the code once in a while. I just updated it to their latest changes.

### How to use
You can either create a new bookmark and paste the code from the IndieGalaKeyGetter.min.js file into the link part of the bookmark or check out my blog post (here)[http://www.thomasle.info/2014/05/indiegala-key-getter] which has a link that you can drag to your bookmark bar, or just view the source to this file and copy the contents of the link into a new bookmark. 

When you are at your bundle link just click the bookmarklet and it will create a textbox and a list of all the games, their cd keys and the store links.

### 13May14 - v1.0

Initial Release

### Known Issues

* On the android games list the very first game has a list of all the subsequent game names for some reason
